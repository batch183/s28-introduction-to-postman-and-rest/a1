// Item 3
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json))

// Item 4

fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => {
	let map = json.map(todos => todos.title);
	console.log(map);
})

// Item 5 
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log(json))

// Item 6
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => console.log("Title: " + json.title + ", Completed? "+ json.completed))

// Item 7
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},

	body: JSON.stringify({
		completed: true,
		title: "Mop the floor",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// Item 8

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},

	body: JSON.stringify({
		completed: true,
		title: "Sweep the floor",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// Item 9
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},

	body: JSON.stringify({
		title: "Sweep the floor",
		description: "Sweep the whole lobby",
		completed: true,
		dateCompleted: "June 20",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// Item 10
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},

	body: JSON.stringify({
		title: "Sweep the floor"
		
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

//Item 11
let today = new Date();
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},

	body: JSON.stringify({
		completed: true,
		dateCompleted: today
		
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// Item 12
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE",
})